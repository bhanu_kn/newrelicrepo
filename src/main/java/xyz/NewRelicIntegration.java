package xyz;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

// for the demo, adding a comment
@RestController
public class NewRelicIntegration {
	@RequestMapping(value = "/hi",method = RequestMethod.POST,
	          consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String newRelicWebhook() {
		System.out.println("Hi");
		return "hi";
		
	}
	 @RequestMapping(value ="/hooks",method = RequestMethod.POST,produces = "application/json", consumes = "application/json")
	    @ResponseBody
	public void webhooks(@RequestBody String jsonBody)
    {
    	JSONParser parse = new JSONParser();
    	try {
			JSONObject jobj = (JSONObject)parse.parse(jsonBody);
			System.out.println(jobj);
			String accName =  (String) jobj.get("account_name");
			System.out.println(accName);
			String details =  (String) jobj.get("details");
			System.out.println(details);
			
			CreateIssue create = new CreateIssue();
			create.creatingIssue(details);
    		}
    	catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
